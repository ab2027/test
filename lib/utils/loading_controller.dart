import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoadingController {
  static final LoadingController _singletonInstance =
      LoadingController._internal();

  factory LoadingController() {
    return _singletonInstance;
  }

  LoadingController._internal();

  showLoading(BuildContext context, {String title = 'Please wait...'}) {
    return _showDialog(context, title);
  }

  hideLoading(BuildContext context) {
    Navigator.of(context).pop();
  }

  _showDialog<T>(BuildContext context, String title) {
    return Platform.isIOS
        ? showCupertinoDialog<T>(
            builder: (context) =>
                CupertinoAlertDialog(content: LoadingWidget(text: title)),
            context: context)
        : showDialog<T>(
            barrierDismissible: false,
            context: context,
            builder: (context) => WillPopScope(
                onWillPop: () async => false,
                child: AlertDialog(content: LoadingWidget(text: title))));
  }
}

class LoadingWidget extends StatelessWidget {
  final String text;

  LoadingWidget({this.text});

  @override
  Widget build(BuildContext context) {
    return Wrap(
      alignment: WrapAlignment.center,
      spacing: 8,
      runSpacing: 8,
      children: <Widget>[
        Platform.isIOS
            ? const CupertinoActivityIndicator()
            : const CircularProgressIndicator(),
        SizedBox(
          width: 24,
        ),
        Container(
          margin: EdgeInsets.only(top: 8),
          child: Text(
            text ?? '',
            textAlign: TextAlign.center,
          ),
        ),
      ],
    );
  }
}
