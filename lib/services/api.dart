import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;

import 'models/album_item.model.dart';

class ApiService {
  static final ApiService _singleton = new ApiService._internal();

  factory ApiService() {
    return _singleton;
  }

  int id = 0;

  ApiService._internal();

  var client = http.Client();

  final String API_ALBUMS_PHOTOS =
      'https://jsonplaceholder.typicode.com/albums/1/photos';

  List<AlbumItem> items = List();

  Future<List<AlbumItem>> getAlbumItems() async {
    if (items.length > 0) {
      return items;
    }

    var data = await client
        .get('https://jsonplaceholder.typicode.com/albums/1/photos');
    var body = jsonDecode(data.body);
    List<AlbumItem> list =
        body.map<AlbumItem>((tagJson) => AlbumItem.fromJson(tagJson)).toList();
    items.addAll(list);
    id = items[items.length - 1].id;
    return items;
  }

  Future updateItem(AlbumItem item) async {
    Map request = item.toJson();

    var data = await client
        .put('https://jsonplaceholder.typicode.com/albums/1/', body: request);
    if (data.statusCode == 200) {
      items[item.id] = item = AlbumItem.fromJson(jsonDecode(data.body));
    }
  }

  Future addItem(AlbumItem item) async {
    item.id = ++id;
    item.albumId = "1";
    Map request = item.toJson();

    var data = await client.post(
        'https://jsonplaceholder.typicode.com/albums/1/photos',
        body: request);
    if (data.statusCode == 201) {
       items.insert(0, item);
    }
  }
}
