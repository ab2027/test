import 'package:flutter/material.dart';
import 'package:upstreet_flutter_code_challenge/screens/album_edit_entity_details.dart';
import 'package:upstreet_flutter_code_challenge/services/models/album_item.model.dart';

class AlbumEntityDetails extends StatefulWidget {
  final AlbumItem album;

  AlbumEntityDetails(this.album);

  @override
  _AlbumEntityDetailsState createState() => _AlbumEntityDetailsState();
}

class _AlbumEntityDetailsState extends State<AlbumEntityDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${widget.album.id}"),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          children: [
            Hero(
              tag: widget.album.id,
              child: Container(
                margin: EdgeInsets.all(32),
                alignment: Alignment.topCenter,
                width: 150,
                height: 150,
                child: Image.network(widget.album.thumbnailUrl, errorBuilder:
                    (BuildContext context, Object exception,
                        StackTrace stackTrace) {
                  return Icon(
                    Icons.broken_image_outlined,
                    color: Colors.red,
                    size: 150,
                  );
                }),
              ),
            ),
            SizedBox(
              height: 16,
            ),
            Container(
              child: Text(
                widget.album.title,
                style: TextStyle(fontSize: 24),
                textAlign: TextAlign.center,
              ),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.edit),
        onPressed: () async {
          var data = await Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) =>
                      AlbumEditEntityDetails(widget.album)));
          if (data != null) {
            Navigator.pop(context, data);
          }
        },
      ),
    );
  }
}
