import 'package:flutter/material.dart';
import 'package:upstreet_flutter_code_challenge/services/api.dart';
import 'package:upstreet_flutter_code_challenge/services/models/album_item.model.dart';
import 'package:upstreet_flutter_code_challenge/utils/loading_controller.dart';

class AlbumEditEntityDetails extends StatefulWidget {
  final AlbumItem album;

  AlbumEditEntityDetails(this.album);

  @override
  _AlbumEditEntityDetailsState createState() => _AlbumEditEntityDetailsState();
}

class _AlbumEditEntityDetailsState extends State<AlbumEditEntityDetails> {
  AlbumItem album;

  @override
  void initState() {
    if (widget.album != null) {
      album = widget.album;
    } else {
      album = new AlbumItem();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${album.id ?? "Add"}"),
        centerTitle: true,
      ),
      body: Container(
        padding: EdgeInsets.all(32),
        child: Column(
          children: [
            TextField(
              controller: TextEditingController(text: album.thumbnailUrl ?? ""),
              decoration: InputDecoration(
                hintText: "url",
              ),
              onChanged: (value) {
                album.thumbnailUrl = value;
              },
            ),
            SizedBox(
              height: 32,
            ),
            TextField(
                controller: TextEditingController(text: album.title ?? ""),
                decoration: InputDecoration(
                  hintText: "Title",
                ),
                onChanged: (value) {
                  album.title = value;
                }),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.done),
        onPressed: () async {
          LoadingController().showLoading(context);
          try {
            if (widget.album == null) {
              await ApiService().addItem(album);
            } else {
              await ApiService().updateItem(album);
            }
            LoadingController().hideLoading(context);
          } finally {
            Navigator.pop(context, album);
          }
        },
      ),
    );
  }
}
