import 'package:flutter/material.dart';
import 'package:upstreet_flutter_code_challenge/services/models/album_item.model.dart';

import '../services/api.dart';
import 'album_edit_entity_details.dart';
import 'album_entity_details.dart';

// TODO:
// 1. Create a list view to display the album data from the fetching function in `api.dart`
// 2. The item of the list should contain the album's thumbnail and title

class AlbumList extends StatefulWidget {
  const AlbumList();

  @override
  _AlbumListState createState() => _AlbumListState();
}

class _AlbumListState extends State<AlbumList> {
  List<AlbumItem> list;
  ScrollController controller = ScrollController();

  @override
  void initState() {
    super.initState();
    getData();
  }

  getData() async {
    list = await ApiService().getAlbumItems();
    setState(() {});
    await Future.delayed(Duration(milliseconds: 200));
    controller.animateTo(0,
        duration: Duration(milliseconds: 200), curve: Curves.ease);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Album List'),
          centerTitle: true,
        ),
        body: list != null
            ? ListView.builder(
                controller: controller,
                itemBuilder: (BuildContext context, int i) {
                  return _AlbumWidget(Key("${list[i].id}"), list[i]);
                },
                itemCount: list.length)
            : Container(),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: addItem,
        ));
  }

  Future addItem() async {
    var result = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) => AlbumEditEntityDetails(null)));
    if (result != null) {
      getData();
    }
  }
}

class _AlbumWidget extends StatefulWidget {
  final AlbumItem album;
  final Key key;

  _AlbumWidget(this.key, this.album);

  @override
  __AlbumWidgetState createState() => __AlbumWidgetState();
}

class __AlbumWidgetState extends State<_AlbumWidget> {
  AlbumItem album;

  @override
  void initState() {
    album = widget.album;
    super.initState();
  }

  openAlbum() async {
    var data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) => AlbumEntityDetails(album)));
    if (data != null) {
      album = data;
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8),
      child: InkWell(
        onTap: openAlbum,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Hero(
              tag: album.id,
              child: Container(
                width: 75,
                height: 75,
                child: Image.network(
                  album.thumbnailUrl,
                  errorBuilder: (BuildContext context, Object exception,
                      StackTrace stackTrace) {
                    return Icon(
                      Icons.broken_image_outlined,
                      color: Colors.red,
                      size: 60,
                    );
                  },
                  loadingBuilder: (BuildContext context, Widget child,
                      ImageChunkEvent loadingProgress) {
                    if (loadingProgress == null) return child;
                    return Center(
                      child: CircularProgressIndicator(
                        value: loadingProgress.expectedTotalBytes != null
                            ? loadingProgress.cumulativeBytesLoaded /
                                loadingProgress.expectedTotalBytes
                            : null,
                      ),
                    );
                  },
                ),
              ),
            ),
            SizedBox(
              width: 8,
            ),
            Expanded(
              child: Text(
                album.title,
                textAlign: TextAlign.left,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
